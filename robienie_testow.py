#!/usr/bin/env python
# coding: utf-8

# In[11]:


import os
import sys


# In[17]:

if (sys.argv[1] == '1'):
    os.system("rm kasa")
    if (os.system("./kompilation.sh") != 0):
        print("\n\nFAIL nie kompiluje sie\n\n")
    else:
        print("\n\nOK skompilowalo sie\n\n")
elif (sys.argv[1] == '0'):
    print("mam nie kompilowac to nie kompiluje")
else:
    print("zla flaga")
    assert (False)


# In[13]:


folder_testow = "testy_python"


# In[14]:


lista_testow = os.listdir(folder_testow + "/")

lista_poprawnych_testow = []

# In[60]:


class Our_fail(Exception):
    pass

def dokonaj_porownania_z_odpowiedzia(miejsce, nazwa_testu_tego):
    def print_poprawiony(message):
        print("\n\n--------------------------------------------------------\n{}\n\n".format(nazwa_testu))
        print(message)

    def read_file(name):
        name = "{}/{}".format(miejsce, name)
        with open (name, "r") as myfile:
            read_lines = myfile.readlines()
        res = []
        for line in read_lines:
            if (line[-1] != '\n'):
                print_poprawiony("gdzies linia jest niezakonczona \n")
                raise Our_fail()
            res.append(line[:-1])
        return res
    out = None
    err = None
    inn = None
    our_err = None
    our_out = None
    try:
        out = read_file("t.out")
        err = read_file("t.err")
        inn = read_file("t.in")
        our_err = read_file("t.our_err")
        our_out = read_file("t.our_out")
    except FileNotFoundError:
        print_poprawiony("FAIL ktoregos z plikow nie ma, moze nie uzupelniles ich")
        return
    except Our_fail:
        return
    
    
    def find_number_in_error(line):
        colon_place = line.find(':')
        if (colon_place == -1):
            print_poprawiony("nie bylo : w error wiadomosci")
            raise Our_fail()
        if (len(line) < 14):
            print_poprawiony("za krotka error wiadomosc")
            raise Our_fail()
        number = int(line[14:colon_place])
        wrong_lines.add(number - 1)
    
    
    def encode_price(price_str):
        dot_pos = price_str.find('.')
        if (dot_pos == -1):
            return 100 * int(price_str)
        return 100 * int(price_str[0: dot_pos]) + int(price_str[dot_pos + 1:])
    
    def encode_time(time_str):
        dot_pos = time_str.find(':')
        if (dot_pos == -1):
            print_poprawiony("nie bylo : w time stringu")
            raise Our_fail()
        return 60 * int(time_str[0: dot_pos]) + int(time_str[dot_pos + 1:])
    
    try:
        if (len(our_err) != len(err)):
            print_poprawiony("rozne liczby bledow w err")
            raise Our_fail()
        for nr_lini_bledu, (line_our_err, line_err) in enumerate(zip(our_err, err)):
            if (line_our_err != line_err):
                print_poprawiony("linia err nr {} jest inna".format(nr_lini_bledu + 1))
                raise Our_fail()
        wrong_lines = set()
        
        for line in err:
            find_number_in_error(line)

        tickets = {}
        routes = {}
        num_of_found_requests = 0
        num_of_used_tickets = 0
        
        for line_nr, line in enumerate(inn):
            if (line_nr in wrong_lines):
                continue

            splited = line.split()

            if (line == ""):
                continue

            if (line[0].isdigit()):
                nr_lini = int(splited[0])
                nowa_route = {}

                i = 1
                while (i < len(splited)):
                    godzina = encode_time(splited[i])
                    przystanek = splited[i + 1]
                    nowa_route[przystanek] = godzina
                    i += 2

                routes[nr_lini] = nowa_route
                continue

            if (line[0] == '?'):
                if (out[num_of_found_requests][0] == ':'):
                    if (out[num_of_found_requests] != our_out[num_of_found_requests]):
                        raise Our_fail()
                    num_of_found_requests += 1
                    continue

                i = 1
                przystanki = []
                numery_lini = []
                while (i < len(splited)):
                    przystanki.append(splited[i])
                    if (i + 1 < len(splited)):
                        numery_lini.append(int(splited[i + 1]))
                    i += 2

                start_time = routes[numery_lini[0]][przystanki[0]]
                end_time = routes[numery_lini[-1]][przystanki[-1]]

                time_of_commute = end_time - start_time + 1
                # tak jest, bo jest to glupie cos w zadaniu zmienione


                def find_time_price_of_set(line):
                    tickets_list = line[2:].split('; ')
                    length = 0
                    price = 0

                    for tic in tickets_list:
                        if (not (tic in tickets)):
                            print_poprawiony("nie ma takiego biletu, cos jest nie tak z out oraz our_out, lub grubszy problem")
                            raise Our_fail()
                        pair = tickets[tic]
                        length += pair[0]
                        price += pair[1]
                    return length, price

                out_pair = find_time_price_of_set(out[num_of_found_requests])
                our_out_pair = find_time_price_of_set(our_out[num_of_found_requests])

                if (out_pair[1] != our_out_pair[1]):
                    print_poprawiony("wartosc ceny zbioru biletow jest niezgodna, linia {}".format(line_nr + 1))
                    raise Our_fail()

                if (our_out_pair[0] < time_of_commute):
                    print_poprawiony("znalezione bilety maja za krotka sume czasu, linia {}".format(line_nr + 1))
                    raise Our_fail()

                num_of_used_tickets += len(our_out[num_of_found_requests][2:].split('; '))
                num_of_found_requests += 1
                continue
            # wiec to musi byc bilet
            czas = int(splited[len(splited) - 1])
            price = encode_price(splited[len(splited) - 2])
            penultimate_space = line[:line.rfind(' ')].rfind(' ')
            nazwa = line[:penultimate_space]
            tickets[nazwa] = (czas, price)
            continue
        if (int(our_out[num_of_found_requests]) != num_of_used_tickets):
            print_poprawiony("zla suma biletow w out")
            raise Our_fail()
    except Our_fail:
        print_poprawiony ("FAIL nie zgadza sie z dostarczonym")
        return
    
    lista_poprawnych_testow.append(nazwa_testu_tego)
    


# In[ ]:





# In[ ]:





# In[ ]:





# In[61]:


for nazwa_testu in lista_testow:
    os.system("rm {}/{}/t.our_out".format(folder_testow, nazwa_testu))
    os.system("rm {}/{}/t.our_err".format(folder_testow, nazwa_testu))
    
    sciezka = "{}/{}".format(folder_testow, nazwa_testu)
    tresc_komendy = "./kasa < {}/t.in > {}/t.our_out 2> {}/t.our_err".format(sciezka, sciezka, sciezka)
    kod = os.system(tresc_komendy)
    if (kod != 0):
        print("\n\n--------------------------------------------------------\n{}\n\n".format(nazwa_testu))
        print("FAIL test {} sie jakos wywalil, abortowal czy cos".format(nazwa_testu))
        continue
    
    dokonaj_porownania_z_odpowiedzia(sciezka, nazwa_testu)


print("\n\nlista testow, na ktorych wszystko bylo dobrze\n\n")

print(lista_poprawnych_testow)

    


# In[ ]:





# In[ ]:




